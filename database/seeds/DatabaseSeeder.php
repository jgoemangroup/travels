<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call([
             CitySeeder::class,
             TransportSeeder::class,
             TravelSeeder::class,
             StepSeeder::class
         ]);
    }


}
class CitySeeder extends Seeder {
    static $places = [
        'Stockholm',
        'New York JFK',
        'Barcelona',
        'Gerona Airport',
        'Madrid',
        'Grasse',
        'Cannes',
        'Nice Riquier',
        'Nice',
        'Paris',
        'Londres',
        'Hogwarts Castle'
    ];
    public function run()
    {
        foreach (self::$places as $place) {
            DB::table('city')->insert([
                'name' => $place]);
        }
    }

}

class TransportSeeder extends Seeder {
    static $type = [
        'plane',
        'Bus',
        'train',
    ];

    public function run()
    {
        foreach (self::$type as $type) {
            DB::table('transport')->insert([
                'name' => $type]);
        }
    }

}

class TravelSeeder extends Seeder {
    public function run() {
        DB::table('Travel')->insert([
            'name' => 'Voyage 1']);
        DB::table('Travel')->insert([
            'name' => 'Voyage 2']);
    }
}

class StepSeeder extends Seeder {
    public function run() {
        for($idTravel = 1;$idTravel <= 2; $idTravel ++) {
            while(\App\Step::where('id_travel', $idTravel)->get()->count() < 6) {
                $idCityStart = \App\City::all()->random(1)->first()->id;
                $idCityStop = \App\City::all()->random(1)->first()->id;
                while($idCityStop === $idCityStart) {
                    $idCityStop = \App\City::all()->random(1)->first()->id;
                }
                try{
                    factory(\App\Step::class, 10)->create(['id_city_start' => $idCityStart, 'id_city_stop' => $idCityStop]);
                }catch(Exception $e){
                    \Illuminate\Support\Facades\Log::alert('Step Seeder => integrity constraint');
                }
            }


        }

    }
}
