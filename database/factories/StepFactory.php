<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Step;
use Faker\Generator as Faker;

$factory->define(Step::class, function (Faker $faker) {
    $idTravel = \App\Travel::all()->random(1)->first()->id;
    $idTransport = \App\Transport::all()->random(1)->first()->id;

    $idCityStop = \App\City::all()->random(1)->first()->id;;
    $stepOrder = random_int(1, 100);
    $ticketNumber = $faker->bothify('******');
    $seat = $faker->bothify('***');
    $gate = $faker->bothify('**');
    $baggageDrop = $faker->bothify('***');





    return [
        'id_travel' => $idTravel,
        'id_city_stop' => $idCityStop,
        'id_transport' => $idTransport, // secret
        'step_order' => $stepOrder,
        'ticket_number' => $ticketNumber,
        'seat' => $seat,
        'gate' => $gate,
        'baggage_drop' => $baggageDrop
    ];
});
