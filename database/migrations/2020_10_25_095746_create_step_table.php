<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStepTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('step', function (Blueprint $table) {
            $table->integer('id_travel');
            $table->integer('id_city_start');
            $table->integer('id_city_stop');
            $table->integer('id_transport');
            $table->foreign('id_travel')->references('id')->on('travel');
            $table->foreign('id_city_start')->references('id')->on('city');
            $table->foreign('id_city_stop')->references('id')->on('city');
            $table->foreign('id_transport')->references('id')->on('transport');
            $table->integer('step_order');
            $table->string('ticket_number');
            $table->string('seat')->nullable();
            $table->string('gate')->nullable();
            $table->string('baggage_drop')->nullable();
            $table->primary(['id_travel', 'id_city_start']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('step');
    }
}
