<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>
    <!-- bootstrap css link -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.2.1/css/bootstrap.min.css">
    <!-- fontawesome css link -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <!-- main jquery library js file -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.2.1/js/bootstrap.min.js"></script>
    <!-- Styles -->
    <link rel="stylesheet" href="{{ URL::asset('css/main.css') }}?build={{ date('m-d-Y') }}">
</head>
<body>
<div class="flex-center position-ref full-height">
    <div class="links">
        <a href="/travels">Consulter les voyages</a>
        <a href="/travel">Créer un nouveau voyage</a>
    </div>
</div>

@yield('content')
<script>
    @if (session('message') != '')
    $('#session-message').show();
    setTimeout(function() {
        $('#session-message').hide();
    }, 4000)
    @endif
</script>

</body>
</html>
