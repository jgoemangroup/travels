@extends('layouts.main')

@section('pageTitle', 'Bienvenue')

@section('content')
    <div class="content">
        <div class="title m-b-md">
            Nos voyages
        </div>
        @foreach($travels as $travel)
            <h2>{{ $travel->name }}</h2>
            @if(count($travel->steps) > 0)
                <h4>Etapes de voyage :</h4>
                @foreach($travel->steps as $step)
        {{ $loop->index + 1 }} : {{ $step->city->name }} à {{ $step->destination->name }}<br>
                @endforeach
            @endif
            <a href="travel/{{ $travel->id }}"> Editer / voir le détail</a>
        @endforeach

    </div>
@endsection
