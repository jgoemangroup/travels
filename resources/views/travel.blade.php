@extends('layouts.main')

@section('pageTitle', 'Bienvenue')

@section('content')
    <div class="content">
        <div class="title m-b-md">
            @if(isset($travel->id))
                Edition du voyage N° {{ $travel->id }}
            @else
                Création d'un voyage
            @endif
        </div>

        <div class="container">
            @if(Session::has('message'))
                <div id="session-message" class="alert alert-dark" role="alert">
                    {{ Session::get('message')}}
                </div>
            @endif
            <form method="POST" action="/travel/edit" class="cmn-form">
                @csrf
                @if(isset($travel->id)) <input type="hidden" name="id" value="{{ $travel->id }}"> @endif
                <div class="row">
                    <div class="col-4">Nom du voyage</div>
                    <div class="col-8"><input type="text" name="name" value="@if(isset($travel->id)) {{ $travel->name }} @endif"/></div>
                </div>
                <button type="submit">Valider</button>
            </form>
            <br><br>
            <h2>Les étapes</h2>
            @if(isset($travel->id))
                    <a class="btn btn-primary" data-toggle="collapse" href="#createCollapse" role="button" aria-expanded="false" aria-controls="createCollapse">
                        Créer une nouvelle étape
                    </a>
                <div class="collapse" id="createCollapse">
                    <form method="POST" action="/step/create" class="cmn-form">
                        <input type="hidden" name="id_travel" value="{{ $travel->id }}">
                        @csrf
                        <div class="row">
                            <div class="col-4">
                                Order étape
                            </div>
                            <div class="col-8">
                                <input type="number" name="step_order" required>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-4">
                                Ville de départ
                            </div>
                            <div class="col-8">
                                <select name="id_city_start">
                                    @foreach($cities as $city)
                                        <option value="{{ $city->id }}">{{ $city->name }}</option>
                                    @endforeach

                                </select>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col-4">
                                Ville d'arrivée
                            </div>
                            <div class="col-8">
                                <select name="id_city_stop" required>
                                    @foreach($cities as $city)
                                        <option value="{{ $city->id }}">{{ $city->name }}</option>
                                    @endforeach

                                </select>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-4">
                                Type de transport
                            </div>
                            <div class="col-8">
                                <select name="id_transport" required>
                                    @foreach($transports as $transport)
                                        <option value="{{ $transport->id }}">{{ $transport->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-4">
                                Numéro de ticket
                            </div>
                            <div class="col-8">
                                <input type="text" name="ticket_number" required>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-4">
                                Siége
                            </div>
                            <div class="col-8">
                                <input type="text" name="seat">
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-4">
                                Porte
                            </div>
                            <div class="col-8">
                                <input type="text" name="gate">
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-4">
                                Bagages
                            </div>
                            <div class="col-8">
                                <input type="text" name="baggage_drop">
                            </div>
                        </div>
                        <br>
                        <button type="submit">Valider</button>
                    </form> <hr>
                </div>

                    <br>
                @if(count($travel->steps) > 0)
                    <h4>Liste des étapes</h4>
                    @foreach($travel->steps as $step)
                        <form method="POST" action="/step/edit" class="cmn-form">
                            <input type="hidden" name="id_travel" value="{{ $travel->id }}">
                            @csrf
                            <div class="row">
                                <div class="col-4">
                                    Order étape
                                </div>
                                <div class="col-8">
                                    <input type="number" name="step_order" value="{{ $step->step_order }}" required>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-4">
                                    Ville de départ
                                </div>
                                <div class="col-8">
                                    <select name="id_city_start">
                                        @foreach($cities as $city)
                                            <option value="{{ $city->id }}" @if($city->id === $step->city->id) selected @endif>{{ $city->name }}</option>
                                        @endforeach

                                    </select>
                                </div>
                            </div><br>
                            <div class="row">
                                <div class="col-4">
                                    Ville d'arrivée
                                </div>
                                <div class="col-8">
                                    <select name="id_city_stop" required>
                                        @foreach($cities as $city)
                                            <option value="{{ $city->id }}" @if($city->id === $step->destination->id) selected @endif>{{ $city->name }}</option>
                                        @endforeach

                                    </select>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-4">
                                    Type de transport
                                </div>
                                <div class="col-8">
                                    <select name="id_transport" required>
                                        @foreach($transports as $transport)
                                            <option value="{{ $transport->id }}" @if($transport->id === $step->transport->id) selected @endif>{{ $transport->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-4">
                                    Numéro de ticket
                                </div>
                                <div class="col-8">
                                    <input type="text" name="ticket_number" value="{{ $step->ticket_number }}" required>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-4">
                                    Siége
                                </div>
                                <div class="col-8">
                                    <input type="text" name="seat" value="{{ $step->seat }}">
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-4">
                                    Porte
                                </div>
                                <div class="col-8">
                                    <input type="text" name="gate" value="{{ $step->gate }}">
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-4">
                                    Bagages
                                </div>
                                <div class="col-8">
                                    <input type="text" name="baggage_drop" value="{{ $step->baggage_drop }}">
                                </div>
                            </div>
                            <br>
                            <button type="submit">Modifier</button>
                        </form>
                    @endforeach
                @endif

            @endif

        </div>
    </div>
@endsection
