<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/**
 * TRAVELS
 */
Route::get('/travels', 'TravelController@all')->name('allTravels');
Route::get('/travel/{id}', 'TravelController@one')->name('seeTravel');
Route::get('/travel', 'TravelController@one')->name('newTravel');
Route::post('/travel/edit', 'TravelController@edit')->name('editTravel');

/**
 * STEPS
 */
Route::post('/step/create', 'StepController@create')->name('createStep');
Route::post('/step/edit', 'StepController@edit')->name('editStep');

