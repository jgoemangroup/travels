<?php

namespace App\Http\Controllers;

use App\Step;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class StepController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Création / edition
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Request $request)
    {
        try {
            Log::info('[StepController@edit][init] Création d\'une nouvelle étape de voyage');

            $validator = Validator::make($request->all(), [
                'id_city_start' => ['required', 'integer', 'min:0'],
                'id_city_stop' => ['required', 'integer', 'min:0'],
                'id_transport' => ['required', 'integer', 'min:0'],
                'step_order' => ['required', 'integer', 'min:0'],
                'ticket_number' => ['required', 'string', 'min:1', 'max:35'],
                'seat' => ['string', 'min:1', 'max:35'],
                'gate' => ['string', 'min:1', 'max:35'],
                'baggage_drop' => ['nullable', 'string', 'min:0', 'max:35'],
            ]);

            if ($validator->fails()) {
                $error = 'Une erreur s\'est produite. Vérifiez les informations saisies.';
                Log::error($error);
                Log::error($validator->getMessageBag());
                return ['message' => 'Une erreur s\'est produite.'];
            } else {
                $datas = [
                    'id_travel' => $request['id_travel'],
                    'id_city_start' => $request['id_city_start'],
                    'id_city_stop' => $request['id_city_stop'],
                    'id_transport' => $request['id_transport'],
                    'step_order' => $request['step_order'],
                    'ticket_number' => $request['ticket_number'],
                    'seat' => $request['seat'],
                    'gate' => $request['gate'],
                    'baggage_drop' => $request['baggage_drop'],
                ];
                $travel = Step::create($datas);
                $travel->save();

                Log::info('[StepController@edit] L\'étape de voyage a bien été créée !');
                session()->flash('message', 'Etape de voyage créée avec succès !');


                return redirect()->route('seeTravel', ['id' => $request['id_travel']]);
            }

            return Redirect::back();
        } catch (\Illuminate\Database\QueryException $e) {
            $error = '[StepController@edit] Une erreur s\'est produite lors de l\'édition d\'une étape de voyage.';
            Log::error($error);
            Log::error($e);
            session()->flash('message', 'Une erreur s\'est produite.');
            return Redirect::back();
        }
    }

    /**
     * Création / edition
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Request $request){
        try{
            Log::info('[StepController@edit][init] Edition d\'une étape de voyage');

            $validator = Validator::make($request->all(), [
                'id_city_start' => ['required', 'integer', 'min:0'],
                'id_city_stop' => ['required', 'integer', 'min:0'],
                'id_transport' => ['required', 'integer', 'min:0'],
                'step_order' => ['required', 'integer', 'min:0'],
                'ticket_number' => ['required', 'string', 'min:1', 'max:35'],
                'seat' => [ 'string', 'min:1', 'max:35'],
                'gate' => [ 'string', 'min:1', 'max:35'],
                'baggage_drop' => ['nullable', 'string', 'min:0', 'max:35'],
            ]);

            if ($validator->fails()) {
                $error = 'Une erreur s\'est produite. Vérifiez les informations saisies.';
                Log::error($error);
                Log::error($validator->getMessageBag());
                return ['message' => 'Une erreur s\'est produite.'];
            } else {
                $step = Step::where([
                    'id_travel' => $request['id_travel'],
                    'id_city_start' => $request['id_city_start']
                ])->first();

                $step->id_travel = $request['id_travel'];
                $step->id_city_start = $request['id_city_start'];
                $step->id_city_stop = $request['id_city_stop'];
                $step->id_transport = $request['id_transport'];
                $step->step_order = $request['step_order'];
                $step->ticket_number = $request['ticket_number'];
                $step->seat = $request['seat'];
                $step->gate = $request['gate'];
                $step->baggage_drop = $request['baggage_drop'];
                $step->save();
                Log::info('[StepController@edit] L\'étape de voyage a bien été mise à jour !');
                session()->flash('message', 'Etape de voyage mise à jour avec succès !');

                return redirect()->route('seeTravel', ['id' => $request['id_travel']]);
            }

            return Redirect::back();
        }catch(\Illuminate\Database\QueryException $e){
            $error = '[StepController@edit] Une erreur s\'est produite lors de l\'édition d\'une étape de voyage.';
            Log::error($error);
            Log::error($e);
            session()->flash('message', 'Une erreur s\'est produite.');
            return Redirect::back();
        }
    }
}
