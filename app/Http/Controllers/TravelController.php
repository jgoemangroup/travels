<?php

namespace App\Http\Controllers;

use App\City;
use App\Http\Resources\TravelResource;
use App\Step;
use App\Transport;
use App\Travel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class TravelController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Get all travels view
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function all(){
        try{
            Log::info('[TravelController@all][init]');
            $travels = Travel::all();
            return view('travels', ['travels' => $travels]);
        }catch(\Illuminate\Database\QueryException $e){
            $error = '[TravelController@all] Une erreur s\'est produite lors de la récupération des voyages.';
            Log::error($error);
            Log::error($e);
            session()->flash('message', 'Une erreur s\'est produite.');
            return Redirect::back();
        }
    }

    /**
     * See one travel or new travel
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function one(Request $request){
        try{
            if(isset($request['id'])) {
                Log::info('[TravelController@one][init] Get Travel N°' . $request['id']);
                $travel = Travel::find($request['id']);
                $cities = City::all();
                $transports = Transport::all();
                return view('travel', ['travel' => $travel, 'cities' => $cities, 'transports' => $transports]);
            } else {
                Log::info('[TravelController@one][init] New travel');
                return view('travel');

            }

        }catch(\Illuminate\Database\QueryException $e){
            $error = '[TravelController@one] Une erreur s\'est produite lors de la récupération  du voyage.';
            Log::error($error);
            Log::error($e);
            session()->flash('message', 'Une erreur s\'est produite.');
            return Redirect::back();
        }
    }

    /**
     * Creat or edit travel
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Request $request){
        try{
            Log::info('[TravelController@edit][init] Création d\'un nouveau voyage / modification d\'un voyage existant');

            $validator = Validator::make($request->all(), [
                'name' => ['required', 'string', 'min:1', 'max:35'],
            ]);

            if ($validator->fails()) {
                $error = 'Une erreur s\'est produite. Vérifiez les informations saisies.';
                Log::error($error);
                Log::error($validator->getMessageBag());
                return ['message' => 'Une erreur s\'est produite.'];
            } else {
                if(isset($request['id'])){
                    $travel = Travel::find($request['id']);
                    $travel->name = $request['name'];
                    $travel->save();
                    Log::info('[TravelController@edit] Le voyage ' . $request['name'] . ' a bien été mis à jour !');
                    session()->flash('message', 'Voyage mis à jour avec succès !');
                } else {
                    $datas = [
                        'name' => $request['name']
                    ];

                    $travel = Travel::create($datas);
                    $travel->save();
                    Log::info('[TravelController@edit] Le voyage ' . $request['name'] . ' a bien été créé !');
                    session()->flash('message', 'Voyage créé avec succès !');
                }

                return redirect()->route('seeTravel', ['id' => $travel->id]);
            }

            return Redirect::back();
        }catch(\Illuminate\Database\QueryException $e){
            $error = '[TravelController@edit] Une erreur s\'est produite lors de l\'édition du voyage.';
            Log::error($error);
            Log::error($e);
            session()->flash('message', 'Une erreur s\'est produite.');
            return Redirect::back();
        }
    }
}
