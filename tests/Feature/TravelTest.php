<?php



namespace Tests\Feature;

use App\Travel;
use Illuminate\Testing\TestView;
use Tests\TestCase;

class TravelTest extends TestCase
{
    public function testTravelsAreCorrectlyListed()
    {
        $response = $this->getJson('/api/apitravels');
        return $response
            ->assertStatus(200)
            ->assertJson([[
                'id' => true,
                'name' => true,
                'created_at' => true,
                'updated_at' => true,
            ]]);
    }

    public function testTravelIsCorrectlyUpdated()
    {
        $datas = [
            'id' => 1,
            'name' => 'test 32',
        ];

        $response = $this->postJson('/api/apitravels/update', $datas);

        return $response
            ->assertStatus(200)
            ->assertJson([
               'success' => true
            ]);
    }

    public function testTravelIsCorrectlyShowed()
    {
        $travel = Travel::find(1);
        $response = $this->getJson('/api/apitravels', ['travel' => $travel]);

        return $response
            ->assertStatus(200)
            ->assertJson([[
                'id' => true,
                'name' => true,
                'created_at' => true,
                'updated_at' => true,
            ]]);
    }
}
